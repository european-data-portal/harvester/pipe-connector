package io.piveau.pipe.connector;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.pipe.model.DataType;
import io.piveau.pipe.model.Endpoint;
import io.piveau.pipe.model.Pipe;
import io.piveau.pipe.utils.PipeManager;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

import java.net.MalformedURLException;
import java.net.URL;

public class PipeContext {

    PipeManager pipeManager;

    private String textResult;
    private byte[] byteResult;
    private String dataMimeType;
    private ObjectNode dataInfo;

    private boolean failure = false;
    private Throwable failureThrowable;

    private boolean forwarded;

    private PipeLogger pipeLogger;

    private PipeConnector pipeConnector;

    PipeContext(Buffer body, PipeConnector pipeConnector) {
        Pipe pipe = Json.decodeValue(body, Pipe.class);
        pipeManager = PipeManager.create(pipe);
        pipeLogger = new PipeLogger(pipe.getHeader(), pipeManager.getCurrentSegment().getHeader());
        this.pipeConnector = pipeConnector;
    }

    PipeContext(JsonObject jsonObject, PipeConnector pipeConnector) {
        Pipe pipe = Json.mapper.convertValue(jsonObject, Pipe.class);
        pipeManager = PipeManager.create(pipe);
        pipeLogger = new PipeLogger(pipe.getHeader(), pipeManager.getCurrentSegment().getHeader());
        this.pipeConnector = pipeConnector;
    }

    public PipeConnector getConnector() {
        return pipeConnector;
    }

    boolean isForwarded() {
        return forwarded;
    }

    public Pipe getPipe() {
        return pipeManager.getPipe();
    }

    public PipeLogger log() {
        return pipeLogger;
    }

    Buffer getFinalPipe() {
        if (textResult != null) {
            pipeManager.setPayloadData(textResult, dataMimeType, dataInfo);
        } else if (byteResult != null) {
            pipeManager.setPayloadData(byteResult, dataMimeType, dataInfo);
        }
        return Json.encodeToBuffer(pipeManager.processedPipe());
    }

    JsonObject getMessage() {
        JsonObject message = new JsonObject().put("config", pipeManager.getConfig());
        if (pipeManager.isBase64Payload()) {
            message
                    .put("data", pipeManager.getBinaryData())
                    .put("dataType", DataType.base64);

        } else {
            message
                    .put("data", pipeManager.getStringData())
                    .put("dataType", DataType.text);
        }

        return message;
    }

    public String getStringData() {
        return pipeManager.getStringData();
    }

    public byte[] getBinaryData() {
        return pipeManager.getBinaryData();
    }

    public String getDataMimeType() {
        return pipeManager.getDataMimeType();
    }

    public ObjectNode getDataInfo() {
        return pipeManager.getDataInfo();
    }

    public JsonNode getConfig() {
        return pipeManager.getConfig();
    }

    public PipeContext setResult(String result) {
        return setResult(result, null, null);
    }

    public PipeContext setResult(String result, String dataMimeType) {
        return setResult(result, dataMimeType, null);
    }

    public PipeContext setResult(byte[] result) {
        return setResult(result, null, null);
    }

    public PipeContext setResult(byte[] result, String dataMimeType) {
        return setResult(result, dataMimeType, null);
    }

    public PipeContext setResult(String result, String dataMimeType, ObjectNode info) {
        this.textResult = result;
        this.dataMimeType = dataMimeType;
        this.dataInfo = info;
        forwarded = false;
        return this;
    }

    public PipeContext setResult(byte[] result, String dataMimeType, ObjectNode info) {
        this.byteResult = result;
        this.dataMimeType = dataMimeType;
        this.dataInfo = info;
        forwarded = false;
        return this;
    }

    public void pass() {
        if (pipeConnector != null) {
            pipeConnector.pass(this);
        }
    }

    public void pass(WebClient client) {
        if (forwarded) {
            log().warn("Already forwarded");
            return;
        }

        if (pipeManager.isBase64Payload()) {
            setResult(getBinaryData(), getDataMimeType(), getDataInfo());
        } else {
            setResult(getStringData(), getDataMimeType(), getDataInfo());
        }
        forward(client);
    }

    public void pass(Vertx vertx) {
        pass(WebClient.create(vertx));
    }

    public void forward() {
        if (pipeConnector != null) {
            pipeConnector.forward(this);
        }
    }

    public void forward(WebClient client) {
        if (forwarded) {
            log().warn("Already forwarded");
            return;
        }

        String logMsg = "forward data";
        if (dataMimeType != null) {
            logMsg += " (" + dataMimeType + "):";
        }
        if (textResult != null) {
            logMsg += "\n" + textResult;
        } else if (byteResult != null) {
            logMsg += "\n" + byteResult.toString();
        } else {
            logMsg += " no data set";
        }
        log().trace(logMsg);

        Endpoint endpoint = getNextEndpoint();
        if (endpoint != null) {
            Buffer pipe = getFinalPipe();
            try {
                URL address = new URL(endpoint.getAddress());
                String method = endpoint.getMethod() != null ? endpoint.getMethod() : "POST";
                client.request(HttpMethod.valueOf(method), address.getPort(), address.getHost(), address.getPath()).putHeader("Content-Type", "application/json").sendBuffer(pipe, ar -> {
                    if (ar.succeeded()) {
                        if (ar.result().statusCode() == 200 || ar.result().statusCode() == 202) {
                            log().trace("successfully forwarded");
                        } else {
                            log().error("{} - {}", ar.result().statusCode(), ar.result().statusMessage());
                        }
                    } else {
                        log().error("Forward request", ar.cause());
                    }
                });
            } catch (MalformedURLException e) {
                log().error("Parsing address", e);
            }
        } else {
            log().trace("No next pipe segment available");
        }
        forwarded = true;
    }

    public void forward(Vertx vertx) {
        forward(WebClient.create(vertx));
    }

    public PipeContext setFailure(String message) {
        return setFailure(new Throwable(message));
    }

    public PipeContext setFailure(Throwable e) {
        this.failure = true;
        this.failureThrowable = e;
        log().error("Pipe failure", e);

        if (pipeConnector != null && pipeConnector.isMailerEnabled()) {
            pipeConnector.useMailer(this).send();
        }

        return this;
    }

    public boolean isFailure() {
        return failure;
    }

    public Throwable getCause() {
        return failureThrowable;
    }

    Endpoint getNextEndpoint() {
        return pipeManager.getNextEndpoint();
    }

}
