package io.piveau.pipe.connector;

import io.piveau.pipe.model.Pipe;
import io.piveau.pipe.model.Segment;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mail.MailAttachment;
import io.vertx.ext.mail.MailClient;
import io.vertx.ext.mail.MailConfig;
import io.vertx.ext.mail.MailMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

class PipeMailer {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private MailClient client;

    private PipeContext pipeContext;

    private JsonObject config;

    private Buffer logo;

    private TemplateEngine templateEngine;

    static PipeMailer create(Vertx vertx, JsonObject config) {
        return new PipeMailer(vertx, config);
    }

    private PipeMailer(Vertx vertx, JsonObject config) {
        this.config = config;
        MailConfig mailConfig = new MailConfig();
        mailConfig.setHostname(config.getString("host"));
        mailConfig.setTrustAll(true);

        client = MailClient.createShared(vertx, mailConfig);

        logo = vertx.fileSystem().readFileBlocking("emails/EDP_LOGO_SMALL.png");

        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setTemplateMode("HTML");
        templateResolver.setPrefix("/emails/");
        templateResolver.setSuffix(".html");

        templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);
    }

    PipeMailer use(PipeContext pipeContext) {
        this.pipeContext = pipeContext;
        return this;
    }

    void send() {
        Pipe pipe = pipeContext.pipeManager.getPipe();
        Segment segment = pipeContext.pipeManager.getCurrentSegment();
        MailMessage message = new MailMessage();
        String address = pipeContext.getConfig().path("mailto").asText(config.getString("mailto", "support@europeandataportal.eu"));
        message.setTo(address);
        message.setFrom("noreply@europeandataportal.eu");
        message.setSubject("[" + pipe.getHeader().getContext() + " - " + config.getString("system", "consus") + "] ["+ segment.getHeader().getName() + "] Event");

        MailAttachment attachment = new MailAttachment();
        attachment.setContentType("image/png");
        attachment.setData(logo);
        attachment.setDisposition("inline");
        attachment.setContentId("logo");
        message.setInlineAttachment(attachment);

        Context ctx = new Context();
        ctx.setVariable("logo", "logo");
        ctx.setVariable("header", segment.getHeader().getTitle());
        ctx.setVariable("harvester", pipe.getHeader().getTitle());
        ctx.setVariable("message", pipeContext.getCause().getMessage());
        ctx.setVariable("details", new JsonObject(segment.getBody().getConfig().toString()).encodePrettily());
        ctx.setVariable("run", pipe.getHeader().getStartTime());

        String content = templateEngine.process("simple", ctx);
        message.setHtml(content);

        client.sendMail(message, ar -> {
            if (ar.succeeded()) {
                log.debug("Mail sent: {}", ar.result().getMessageID());
            } else {
                log.error("Mail sent", ar.cause());
            }
        });
    }

}
