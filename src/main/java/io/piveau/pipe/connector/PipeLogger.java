package io.piveau.pipe.connector;

import io.piveau.pipe.model.PipeHeader;
import io.piveau.pipe.model.SegmentHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Formatter;

public class PipeLogger {

    private Logger log;
    private String prefix;

    PipeLogger(PipeHeader pipe, SegmentHeader segment) {
        log = LoggerFactory.getLogger("pipe." + pipe.getId());

        StringBuilder sb = new StringBuilder();
        Formatter formatter = new Formatter(sb);
        formatter.format("%s [%s (%s)] [%s (%s)] ", pipe.getRunId(), pipe.getTitle(), pipe.getContext(), segment.getTitle(), segment.getName());
        prefix = sb.toString();
    }

    public void error(String message) {
        log.error(prefix + message);
    }

    public void error(String format, Object argument1) {
        log.error(prefix + format, argument1);
    }

    public void error(String format, Object argument1, Object argument2) {
        log.error(prefix + format, argument1, argument2);
    }

    public void error(String format, Object... arguments) {
        log.error(prefix + format, arguments);
    }

    public void error(String format, Throwable e) {
        log.error(prefix + format, e);
    }

    public void warn(String message) {
        log.warn(prefix + message);
    }

    public void warn(String format, Object argument1) {
        log.warn(prefix + format, argument1);
    }

    public void warn(String format, Object argument1, Object argument2) {
        log.warn(prefix + format, argument1, argument2);
    }

    public void warn(String format, Object... arguments) {
        log.warn(prefix + format, arguments);
    }

    public void warn(String format, Throwable e) {
        log.warn(prefix + format, e);
    }

    public void info(String message) {
        log.info(prefix + message);
    }

    public void info(String format, Object argument1) {
        log.info(prefix + format, argument1);
    }

    public void info(String format, Object argument1, Object argument2) {
        log.info(prefix + format, argument1, argument2);
    }

    public void info(String format, Object... arguments) {
        log.info(prefix + format, arguments);
    }

    public void info(String format, Throwable e) {
        log.info(prefix + format, e);
    }

    public void debug(String message) {
        log.debug(prefix + message);
    }

    public void debug(String format, Object argument1) {
        log.debug(prefix + format, argument1);
    }

    public void debug(String format, Object argument1, Object argument2) {
        log.debug(prefix + format, argument1, argument2);
    }

    public void debug(String format, Object... arguments) {
        log.debug(prefix + format, arguments);
    }

    public void debug(String format, Throwable e) {
        log.debug(prefix + format, e);
    }

    public void trace(String message) {
        log.trace(prefix + message);
    }

    public void trace(String format, Object argument1) {
        log.trace(prefix + format, argument1);
    }

    public void trace(String format, Object argument1, Object argument2) {
        log.trace(prefix + format, argument1, argument2);
    }

    public void trace(String format, Object... arguments) {
        log.trace(prefix + format, arguments);
    }

    public void trace(String format, Throwable e) {
        log.trace(prefix + format, e);
    }

}
