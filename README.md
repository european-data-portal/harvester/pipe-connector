# pipe connector

## Table of Contents
1. [Features](#features)
1. [Get Started](#get-started)
1. [Configuration](#configuration)
1. [Build & Install](#build-install)
1. [Use](#use)
1. [License](#license)

## Features
 * Asynchronous event based pipe handling
 * conformable logging
 * Internal management of pipe structure
 * Convenient API
 * Vert.x support

## Get Started

The most simple example would be:
```java
PipeConnector.create().handle(pipeContext -> pipeContext.setResult("Hello World!"));
```

Of course, a more practical example would be this:
```java
PipeConnector connector = PipeConnector.create();
connector.handle(pipeContext -> {
    JsonNode config = pipeContext.getConfig();
    String data = pipeContext.getStringData();

    // do your job here
    String result = ...
    
    pipeContext.setResult(result); 
});
```

If you're already in a Vert.x context and would like to use a worker Verticle with an event bus address, this will do:
```java
PipeConnector.create(vertx, ar -> {
    if (ar.succeeded()) {
        ar.result().consumerAddress(EVENTBUS_ADDRESS);
        startFuture.complete();
    } else {
        startFuture.fail(ar.cause());
    }
});
```

## Configuration

 * PIPE_ENDPOINT_PORT - Default port is 8080
 * PIPE_MAIL_CONFIG - JSON object:
   
   ```json
   {
      "host": "<smtp_host>",
      "system": "<environment>",
      "mailto": "<receiver_address>"
   }
   ```
  
  ## Build & Install
  Requirements:
   * Git
   * Maven
   * Java
  
  ```bash
  $ git clone https://gitlab.com/european-data-portal/harvester/pipe-connector.git
  $ cd pipe-connector
  $ mvn install
  ```
  
  ## Use
  Add dependency to your project pom file:
  ```xml
  <dependency>
      <groupId>io.piveau</groupId>
      <artifactId>pipe-connector</artifactId>
      <version>4.3.1</version>
  </dependency>
  ```
  
  ## License
  
  [Apache License, Version 2.0](LICENSE.md)
