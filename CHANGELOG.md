# ChangeLog

## Unreleased

**Added:**

**Changed:**

**Removed:**

**Fixed:**

## [4.3.1](https://gitlab.com/european-data-portal/harvester/pipe-connector/tags/4.3.1) (2019-05-06)

**Added:**
- EMail support
- Configuration for the release plugin in the pom file
- ChangeLog

**Changed:**
- Upgrade to Vert.x 3.7.0
- Upgrade to Vert.x 3.6.3

**Removed:**

**Fixed:**
